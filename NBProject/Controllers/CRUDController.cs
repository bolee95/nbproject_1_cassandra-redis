﻿using System.Web.Mvc;
using Domain;
using System.Web;
using System.IO;
using System;
using Domain.Entities;
using System.Collections.Generic;

namespace NBProject.Controllers
{
    public class CRUDController : Controller
    {
        public ActionResult Index()
        {
            return View(DataProvider.getAllSongs());
        }

        public ActionResult IndexSinger()
        {
            return View(DataProvider.getAllSingers());
        }
        #region CRUDsong
        public ViewResult CreateSong()
        {
            return View();
        }
        
        public RedirectToRouteResult DeleteSong(string Name)
        {
            Song forDeleting = DataProvider.getSongByName(Name);
            DataProvider.deleteSong(forDeleting);
            DataProvider.DeleteDataByName(Name + forDeleting.Singer);
            return RedirectToAction("Index", "CRUD");
        }

        public ViewResult DetailsSong(string name)
        {
            return View(DataProvider.getSongByName(name));
        }

        public ViewResult UpdateSong(string name)
        {
            return View(DataProvider.getSongByName(name));
        }

        public RedirectToRouteResult UploadSongData(HttpPostedFileBase file, string Name, string Lyrics, string Album, float Price, string Singer, string Category)
        {
            if (file != null)
            {
                if (!(DataProvider.getSongByName(Name) == null))
                {
                    DataProvider.deleteSong(DataProvider.getSongByName(Name));
                    //za pesmu je ime sastavljeno iz naziva i pevaca
                    DataProvider.DeleteDataByName(Name + Singer); 
                }           

                string songName = System.IO.Path.GetFileNameWithoutExtension(file.FileName);
                string mime = System.IO.Path.GetExtension(file.FileName);

                byte[] fileData = null;
                using (var binaryReader = new BinaryReader(Request.Files[0].InputStream))
                {
                    fileData = binaryReader.ReadBytes(Request.Files[0].ContentLength);
                }

                Data newData = new Data();
                Song newSong = new Song();

                newSong.Album = Album;
                newSong.Lyrics = Lyrics;
                newSong.Name = Name;
                newSong.Price = Price;
                newSong.Singer = Singer;
                newSong.Category = Category;

                DataProvider.addNewSong(newSong);

                if (!(fileData.Length > 1048576))
                {
                    newData.Media = fileData;
                    newData.Mime = mime;
                    newData.Name = Name + Singer;

                    DataProvider.InsertData(newData);
                }
                else
                {
                    List<byte[]> chanks = SplitFileAndUpload(fileData);
                    int counter = 0;
                    foreach (byte[] chunk in chanks)
                    {
                        Data newDataChunk = new Data();
                        newDataChunk.Media = chunk;
                        newDataChunk.Mime = mime;
                        newDataChunk.RedniBr = counter;
                        newDataChunk.Name = Name + Singer;

                        DataProvider.InsertData(newDataChunk);
                        counter++;
                    }
                }
            }
            return RedirectToAction("Index", "CRUD");
        }
        #endregion
        #region CRUDsinger
        public ViewResult CreateSinger()
        {
            return View();
        }

        public RedirectToRouteResult DeleteSinger(string Name)
        {
            Singer forDeleting = DataProvider.getSingerByName(Name);
            DataProvider.deleteSinger(forDeleting);
            DataProvider.DeleteDataByName(Name + forDeleting.Alias);
            return RedirectToAction("IndexSinger", "CRUD");
        }

        public ViewResult DetailsSinger(string name)
        {
            return View(DataProvider.getSingerByName(name));
        }

        public RedirectToRouteResult UploadSingerData(HttpPostedFileBase file, string Name, string Alias, string BirthPlace, DateTimeOffset? YearOfBirth = null)
        {
            if (file != null)
            {
                if (!(DataProvider.getSingerByName(Name) == null))
                {
                    DataProvider.deleteSinger(DataProvider.getSingerByName(Name));
                    DataProvider.DeleteDataByName(Name + Alias);
                }
                string songName = System.IO.Path.GetFileNameWithoutExtension(file.FileName);
                string mime = System.IO.Path.GetExtension(file.FileName);

                byte[] fileData = null;
                using (var binaryReader = new BinaryReader(Request.Files[0].InputStream))
                {
                    fileData = binaryReader.ReadBytes(Request.Files[0].ContentLength);
                }
                Data newData = new Data();
                Singer newSinger = new Singer();

                newSinger.Name = Name;
                newSinger.Alias = Alias;
                newSinger.BirthPlace = BirthPlace;
                newSinger.YearOfBirth = (DateTimeOffset) YearOfBirth;
                DataProvider.addNewSinger(newSinger);

                if (!(fileData.Length > 1048576))
                {

                    newData.Media = fileData;
                    newData.Mime = mime;
                    newData.Name = Name + Alias; //Za pevace ce da se cuva naziv kao ime pevaca + alias                  
                    DataProvider.InsertData(newData);
                }
                else
                {
                    List<byte[]> chanks = SplitFileAndUpload(fileData);
                    int counter = 0;
                    foreach (byte[] chunk in chanks)
                    {
                        Data newDataChunk = new Data();
                        newDataChunk.Media = chunk;
                        newDataChunk.Mime = mime;
                        newDataChunk.RedniBr = counter;
                        newDataChunk.Name = Name + Alias;

                        DataProvider.InsertData(newDataChunk);
                        counter++;
                    }
                }

            }
            return RedirectToAction("IndexSinger", "CRUD");
        }


        public ViewResult UpdateSinger(string name)
        {
            return View(DataProvider.getSingerByName(name));
        }

        #endregion

        private List<byte[]> SplitFileAndUpload(byte[] fileData)
        {
            int len = 1048576;
            List<byte[]> chunkArray = new List<byte[]>();

            for (int i = 0; i < fileData.Length;)
            {
                byte[] chunk = new byte[len];
                int j = 0;

                while (++j < chunk.Length && i < fileData.Length)
                {
                    chunk[j] = fileData[i++];
                }
                chunkArray.Add(chunk);
            }
            return chunkArray;
        }
    }
}