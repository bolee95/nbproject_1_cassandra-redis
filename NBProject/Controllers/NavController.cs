﻿using Domain;
using Domain.Entities;
using NBProject.Models;
using Newtonsoft.Json;
using RedisDataLayer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NBProject.Controllers
{
    public class NavController : Controller
    {

        public PartialViewResult Menu(string category = null,string singerSelected = null)
        {

            ViewBag.SelectedCategory = category;
            ViewBag.SingerSelected = singerSelected;

            List<string> kategorije = DataProvider.getAllCategories();
            List<Singer> pevaci = DataProvider.getAllSingers();

            NavModelView newModel = new NavModelView(kategorije.Distinct().ToList(), pevaci);
            return PartialView(newModel);
        }

        public PartialViewResult Profile(string id = null)
        {
            ProfilViewModel model = new ProfilViewModel();
            if (id != null)
            {
                Song songProf = DataProvider.getSongByName(id);
                Singer singerProf = DataProvider.getSingerBySongName(id);
                //Singer singerProf = DataProvider.getSingerByName(songProf.Singer);
                model.singerAlias = singerProf.Alias;
                model.singerBirthYear = singerProf.YearOfBirth;
                model.singerName = singerProf.Name;
                model.concNameAndAlias = model.singerName + model.singerAlias;
                model.songLyrics = songProf.Lyrics;
                model.songName = songProf.Name;
            }

            return PartialView(model);
        }

        public ActionResult GetImage(string concNameAndAlias)
        {
            //Vraca sliku pevaca na osnovu njegovog ida
            if (concNameAndAlias != null)

            {
                if (!DatabaseManager.Exists(concNameAndAlias))
                {
                    Data picture = DataProvider.GetDataByName(concNameAndAlias).FirstOrDefault();
                    if (picture != null)
                    {
                        string singer = JsonConvert.SerializeObject(picture);
                        DatabaseManager.StoreData(concNameAndAlias, singer);
                        return File(picture.Media, picture.Mime);

                    }
                    else
                    {

                        var dir = Server.MapPath("/Content/");
                        var path = Path.Combine(dir, "MusicStore.jpg");
                        return base.File(path, "image/jpeg");
                    }
                }
                else
                {
                    string singer = DatabaseManager.GetData(concNameAndAlias);
                    Data picture = JsonConvert.DeserializeObject<Data>(singer);
                    return File(picture.Media, picture.Mime);
                }
            }
            else
            {
                var dir = Server.MapPath("/Content/");
                var path = Path.Combine(dir, "MusicStore.jpg");
                return base.File(path, "image/jpeg");
            }
        }
    }
}