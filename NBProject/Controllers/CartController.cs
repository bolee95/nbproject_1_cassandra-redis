﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain.Entities;
using Domain;
using NBProject.Models;

namespace NBProject.Controllers
{
    public class CartController : Controller
    {

        public ViewResult Index(string returnUrl)
        {
            return View(new KorpaViewModel
            {
               
                Cart = new Cart(),
                ReturnUrl = returnUrl
            });
        }

        public RedirectToRouteResult AddToCart(string Name, string returnUrl)
        {
            Cart newCart = new Cart();
            Song toCart = DataProvider.getSongByName(Name);
            newCart.AddItem(toCart);
            return RedirectToAction("List", "Song");
            
        }
        public RedirectToRouteResult RemoveFromCart(string Name, string returnUrl)
        {
            Cart newCart = new Cart();
            Song toCart = DataProvider.getSongByName(Name);
            newCart.RemoveLine(toCart);
          
            return RedirectToAction("Index", new { returnUrl });
            
        }
        //private Cart GetCart()
        //{
        //    Cart cart = (Cart)Session["Cart"];
        //    if (cart == null)
        //    {
        //        cart = new Cart();
        //        Session["Cart"] = cart;
        //    }
        //    return cart;
        //}


        public PartialViewResult Summary(Cart cart)
        {
            return PartialView(cart);
        }

        public ViewResult Checkout()
        {
            Cart newChart = new Cart();
            newChart.removeAll();
            return View();
        }
    }
}