﻿using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NBProject.Models;
using Cassandra;
using Cassandra.Data.Linq;
using CqlPoco;
using Domain.Entities;
using RedisDataLayer;

namespace NBProject.Controllers
{
    public class SongController : Controller
    {
        private List<Song> lista = new List<Song>();

        public ViewResult List(string category, string Id = null, string singerSelected = null)
        {
            ViewBag.SingerSelected = singerSelected;
            ViewBag.SelectedCategory = category;

            IEnumerable<Song> listaPesama = new List<Song>();
            if (category != null)
                listaPesama = DataProvider.getSongsByCategory(category);
            else if (singerSelected != null)
                listaPesama = DataProvider.getSongsBySinger(singerSelected);
            else
                listaPesama = DataProvider.getAllSongs();

            ListaPesamaViewModel model = new ListaPesamaViewModel();
            model.Id = Id;
            model.CurrentCategory = category;
            model.CurrentSinger = singerSelected;
            foreach (var song in listaPesama)
            {
                SongAndRating newSong = new SongAndRating();
                newSong.Song = song;
                List<float> rates = DataProvider.getRatesByName(song.Name + song.Singer);
                if (rates.Count == 0)
                    newSong.Rating = 0;
                else
                    newSong.Rating = rates.Average();
                model.listSongAndRating.Add(newSong);
            }


            return View(model);
        }

        public FileContentResult getMediaFile(string concNameAndSinger)
        {
            //Vraca pesmu na osnovu ida
            if (concNameAndSinger != null)
            {
                List<Data> musicParts = DataProvider.GetDataByName(concNameAndSinger);
                if (musicParts.Count != 0)
                {
                    int sum = musicParts.Sum(m => m.Media.Length);
                    byte[] musicBytes = new byte[sum];

                    int offset = 0;
                    foreach (Data part in musicParts)
                    {
                        System.Buffer.BlockCopy(part.Media, 0, musicBytes, offset, part.Media.Length);
                        offset += part.Media.Length;
                    }
                    Data musicFile = new Data();
                    musicFile.Media = musicBytes;
                    musicFile.Mime = musicParts.First().Mime;
                    musicFile.Name = musicParts.First().Name;

                    //DatabaseManager.StoreData(musicFile.Name, System.Text.Encoding.UTF8.GetString(musicFile.Media));

                    return File(musicFile.Media, System.Net.Mime.MediaTypeNames.Application.Octet, musicFile.Name + musicFile.Mime);
                }
                else
                    return null;
            }
            else
                return null;
        }

        public RedirectToRouteResult RatedReturn(string id = null, string value = null)
        {
            if (id != null && value != null)
                DataProvider.rateByName(id, float.Parse(value));
            return RedirectToAction("List", "Song");
        }
    }
}