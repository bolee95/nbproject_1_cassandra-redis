﻿using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NBProject.Models
{
    public class NavModelView
    {
        public List<string> Categories { get; set; }
        public List<Singer> Singers { get; set; }

        public NavModelView(List<string> categories,List<Singer> Singers)
        {
            this.Categories = categories;
            this.Singers = Singers;
        }

        public NavModelView()
        {
        }
    }
}