﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Domain.Entities;

namespace NBProject.Models
{
    public class KorpaViewModel
    {
        public Cart Cart { get; set; }
        public string ReturnUrl { get; set; }
    }
}