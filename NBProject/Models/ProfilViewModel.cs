﻿using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NBProject.Models
{
    public class ProfilViewModel
    {
        public string singerName { get; set; }
        public DateTimeOffset singerBirthYear { get; set; }
        public string singerAlias { get; set; }
        public string songName { get; set; }
        public string songLyrics { get; set; }
        public string concNameAndAlias { get; set; }
        public ProfilViewModel()
        {
        }

    }
}