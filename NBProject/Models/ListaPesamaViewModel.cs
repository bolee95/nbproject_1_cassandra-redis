﻿using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NBProject.Models
{
    public class ListaPesamaViewModel
    {
        public List<SongAndRating> listSongAndRating;
        public string Id { get; set; }
        public string CurrentCategory { get; set; }
        public string CurrentSinger { get; set; }
        public ListaPesamaViewModel()
        {
            listSongAndRating = new List<SongAndRating>();
            Id = null;
            CurrentCategory = null;
            CurrentSinger = null;
        }

    }


    public class SongAndRating
    {
        public Song Song { get; set; }
        public float Rating { get; set; }

        public SongAndRating()
        {
            this.Song = null;
            this.Rating = 1;
        }

    }
}