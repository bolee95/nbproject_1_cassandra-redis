﻿$('.rateit').bind('rated reset', function (e) {
    var ri = $(this);

    var value = ri.rateit('value');
    var productID = ri.data('productid');
    ri.rateit('readonly', true);

    $.ajax({
        url: '/Song/RatedReturn?id=' + productID + '&value=' + value,
        data: { id: productID, value: value },
        type: 'POST',
        success: function (data) {
            $('#' + productID).text('Thank you for voting!')
        }
    });
});

var a = audiojs;
a.events.ready(function () {
    var a1 = a.createAll();
});