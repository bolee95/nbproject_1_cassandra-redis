﻿using Newtonsoft.Json;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace RedisDataLayer
{
    public static class DatabaseManager
    {
        private static ConnectionMultiplexer connectionMultiplexer = null;
        private static IDatabase database = null;


        public static void Configure()
        {
            if (connectionMultiplexer == null || database == null)
            {
                var connectionString = string.Format("{0}:{1}", "127.0.0.1", 6379);
                connectionMultiplexer = ConnectionMultiplexer.Connect(connectionString);
                database = connectionMultiplexer.GetDatabase();
            }
        }



        public static void Add<T>(string key, T value, DateTimeOffset expiresAt) where T : class
        {
            var serializedObject = JsonConvert.SerializeObject(value);
            var expiration = expiresAt.Subtract(DateTimeOffset.Now.AddMinutes(-20));

             database.StringSet(key, serializedObject);
               
        }


        public static bool StoreData(string key, string value)
        {
            return database.StringSet(key, value);
        }

        public static string GetData(string key)
        {
            return database.StringGet(key);
        }

        public static void DeleteData(string key)
        {
            database.KeyDelete(key);
        }

        public static T Get<T>(string key) where T : class
        {
            var serializedObject = database.StringGet(key);
            if (!serializedObject.IsNull)
                return JsonConvert.DeserializeObject<T>(serializedObject);
            else
                return null;
        }

        public static bool Remove(string key)
        {
            return database.KeyDelete(key);
        }

        public static bool Exists(string key)
        {
            return database.KeyExists(key);
        }

        public static bool ListRightPush<T>(T value, string key) where T : class
        {
            var jsonValue = JsonConvert.SerializeObject(value);
            database.ListRightPush(key, jsonValue);

            return true;
        }

        public static string ListRightPop<T>(T value, string key) where T : class
        {
            return database.ListRightPop(key);

        }

        public static bool ListLeftPush<T>(T value, string key) where T : class
        {
            var jsonValue = JsonConvert.SerializeObject(value);
            database.ListLeftPush(key, jsonValue);

            return true;
        }

        public static string ListLefttPop<T>(T value, string key) where T : class
        {
            return database.ListLeftPop(key);

        }
        public static List<T> GetList<T>(string key) where T : class
        {
            RedisValue[] values = database.ListRange(key, 0, 100);
            List<T> objects = new List<T>();
            foreach (RedisValue s in values)
            {
                objects.Add(JsonConvert.DeserializeObject<T>(s));
            }

            return objects;
        }

        public static string ListGetByIndex<T>(string key, long index) where T : class
        {
            return database.ListGetByIndex(key, index);
        }

        public static bool ListRemove<T>(string key, T value) where T : class
        {
            var jsonValue = JsonConvert.SerializeObject(value);
            database.ListRemove(key, jsonValue);
            return true;


        }


    }
}
