﻿using CqlPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    [TableName("rating")]
    [PrimaryKey("name")]
    public class Rating
    {
        [Column("name")]
        public string Name { get; set; }
        [Column("rate")]
        public float Rate { get; set; }
        [Column("randkey")]
        public string RandKey { get; set; }

        public Rating()
        { }

        public Rating(string name, float rate)
        {
            this.Name = name;
            this.Rate = rate;
            this.RandKey = DateTime.Now.ToLocalTime().ToString();
        }
    }
}
