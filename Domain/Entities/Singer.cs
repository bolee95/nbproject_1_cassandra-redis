﻿using CqlPoco;
using System;
using static System.Net.Mime.MediaTypeNames;


namespace Domain
{
    [TableName("singer")]
    [PrimaryKey("name")]
    public class Singer
    {
        [Column("name")]
        public string Name { get; set; }
        [Column("alias")]
        public string Alias { get; set; }
        [Column("yearofbirth")]
        public DateTimeOffset YearOfBirth { get; set; }
        [Column("birthplace")]
        public string BirthPlace { get; set; }

        public Singer(string name,string alias, DateTime date,string birthPlace)
        {
            this.Name = name;
            this.Alias = alias;
            this.YearOfBirth = date;
            this.BirthPlace = birthPlace;
        }

        public Singer()
        {
        }
    }
}
