﻿using CqlPoco;

namespace Domain
{
    [TableName("song")]
    [PrimaryKey("name")]
    public class Song
    {
        [Column("name")]
        public string Name { get; set; }
        [Column("lyrics")]
        public string Lyrics { get; set; }
        [Column("album")]
        public string Album { get; set; }
        [Column("price")]
        public float Price { get; set; }
        [Column("singername")]
        public string Singer { get; set; }
        [Column("category")]
        public string Category { get; set; }

        public Song(string name, string lyrics, string album, float price, string singer,string category)
        {
            this.Name = name;
            this.Lyrics = lyrics;
            this.Album = album;
            this.Price = price;
            this.Singer = singer;
            this.Category = category;
        }

        public Song()
        {
        }
    }
}
