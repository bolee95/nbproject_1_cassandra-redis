﻿using RedisDataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class Cart
    {
        public static string key = "shoppingCARTreddisKEY";
        //private List<Song> SongCollection = new List<Song>();

        public void AddItem(Song song)
        {
            DatabaseManager.ListRightPush(song, Cart.key);
        }

        public void RemoveLine(Song song)
        {
            DatabaseManager.ListRemove<Song>(Cart.key, song);
        }

        public float ComputeTotalValue()
        {
            List<Song> songsInCart = new List<Song>();
            songsInCart = DatabaseManager.GetList<Song>(Cart.key);
            return songsInCart.Sum(x => x.Price);
        }

        public void Clear()
        {
            DatabaseManager.Remove(Cart.key);
        }

        public IEnumerable<Song> Lines
        {
            get { return DatabaseManager.GetList<Song>(Cart.key);}
        }

        public void removeAll()
        {
            DatabaseManager.Remove(Cart.key);
        }
    }


}