﻿using CqlPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    [TableName("media")]
    [PrimaryKey("name")]
    public class Data
    {
        [Column("name")]
        public string Name { get; set; }
        [Column("data")]
        public byte[] Media { get; set; }
        [Column("mime")]
        public string Mime { get; set; }
        [Column("rednibr")]
        public int RedniBr { get; set; }

        public Data()
        {
        }

        public Data(string Name,byte[] Media, string Mime,int RedniBr)
        {
            this.Name = Name;
            this.Media = Media;
            this.Mime = Mime;
            this.RedniBr = RedniBr;
        }
    }
}
