﻿using Cassandra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CqlPoco;
using Domain.Entities;
using RedisDataLayer;

namespace Domain
{
    public static class DataProvider
    {
        #region Song

        public static bool addNewSong(Song song)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return false;

            ICqlClient client = CqlClientConfiguration.ForSession(session).BuildCqlClient();

            client.Insert(song);
            return true;
        }

        public static bool updateSong(Song song)
        {

                ISession session = SessionManager.GetSession();

                if (session == null)
                    return false;

                ICqlClient client = CqlClientConfiguration.ForSession(session).BuildCqlClient();

                client.Insert(song);
                return true;
        }

        public static List<Song> getAllSongs()
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return null;

            ICqlClient client = CqlClientConfiguration.ForSession(session).BuildCqlClient();

            return client.Fetch<Song>("SELECT * FROM song");
        }

        public static Song getSongByName(string name)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return null;

            ICqlClient client = CqlClientConfiguration.ForSession(session).BuildCqlClient();

            return client.SingleOrDefault<Song>("SELECT * FROM song WHERE name = ?", name);
        }


        public static List<Song> getSongsByCategory(string category)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return null;

            ICqlClient client = CqlClientConfiguration.ForSession(session).BuildCqlClient();

            return client.Fetch<Song>("SELECT * FROM song WHERE category = ?", category);
        }

        public static List<string> getAllCategories()
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return null;

            ICqlClient client = CqlClientConfiguration.ForSession(session).BuildCqlClient();
            return client.Fetch<string>("SELECT category FROM song");

        }

        public static List<Song> getSongsBySinger(string singer)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return null;

            ICqlClient client = CqlClientConfiguration.ForSession(session).BuildCqlClient();

            return client.Fetch<Song>("SELECT * FROM song WHERE singername = ?", singer);
        }


        public static bool deleteSong(Song forDeleting)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return false;

            ICqlClient client = CqlClientConfiguration.ForSession(session).BuildCqlClient();

            client.Delete(forDeleting);
            return true;
        }
        #endregion

        #region Singer
        public static List<Singer> getAllSingers()
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return null;

            ICqlClient client = CqlClientConfiguration.ForSession(session).BuildCqlClient();

            return client.Fetch<Singer>("SELECT * FROM singer");
        }


        public static Singer getSingerBySongName(string nameSong)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return null;

            ICqlClient client = CqlClientConfiguration.ForSession(session).BuildCqlClient();
            //izmenjeno
            Song test = client.SingleOrDefault<Song>("SELECT * FROM song WHERE name = ?", nameSong);

            return client.SingleOrDefault<Singer>("SELECT * FROM singer WHERE name = ?", test.Singer);
        }



        public static bool addNewSinger(Singer singer)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return false;

            ICqlClient client = CqlClientConfiguration.ForSession(session).BuildCqlClient();

            client.Insert(singer);
            return true;
        }


        public static Singer getSingerByName(string name)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return null;

            ICqlClient client = CqlClientConfiguration.ForSession(session).BuildCqlClient();

            return client.SingleOrDefault<Singer>("SELECT * FROM singer WHERE name = ?", name);
        }

        public static bool deleteSinger(Singer forDeleting)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return false;

            ICqlClient client = CqlClientConfiguration.ForSession(session).BuildCqlClient();

            client.Delete(forDeleting);
            return true;
        }

        #endregion


        #region Data
        public static bool InsertData(Data mediaFile)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return false;

            ICqlClient client = CqlClientConfiguration.ForSession(session).BuildCqlClient();

            client.Insert(mediaFile);
            return true;
        }
     
        public static List<Data> GetDataByName(string name)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return null;

            ICqlClient client = CqlClientConfiguration.ForSession(session).BuildCqlClient();

            return client.Fetch<Data>("WHERE name = ?", name);

        }


        public static bool DeleteDataByName(string name)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return false;

            ICqlClient client = CqlClientConfiguration.ForSession(session).BuildCqlClient();

            client.Execute("DELETE FROM media WHERE name = ?",name);
            return true;
        }
        #endregion

        #region Rating
        public static List<float> getRatesByName(string name)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return null;

            ICqlClient client = CqlClientConfiguration.ForSession(session).BuildCqlClient();

            return client.Fetch<float>("SELECT rate FROM rating WHERE name = ?", name);
        }

        public static bool rateByName(string name,float rate)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return true;

            ICqlClient client = CqlClientConfiguration.ForSession(session).BuildCqlClient();

            client.Insert(new Rating(name, rate));
            return true;
        }
    }
}
#endregion